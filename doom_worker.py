#!/usr/bin/env python3

# std lib
import time
import random
import os

# 3rd party
from PIL import Image
from vizdoom import DoomGame, ScreenFormat, ScreenResolution
import keyboard
import numpy as np

left = [1, 0, 0, 0]
right = [0, 1, 0, 0]
forward = [0, 0, 1, 0]
shoot = [0, 0, 0, 1]
actions = [shoot, left, right, forward]


def get_current_move():
    return [
        keyboard.is_pressed("left"),
        keyboard.is_pressed("right"),
        keyboard.is_pressed("up"),
        keyboard.is_pressed("ctrl"),
    ]


def doom_worker(framebuffer):
    print("starting doom game")
    # init doom game
    game = DoomGame()
    game.load_config("./basic.cfg")
    game.set_window_visible(False)
    game.set_screen_resolution(ScreenResolution.RES_160X120)
    game.set_screen_format(ScreenFormat.RGB24)
    game.init()
    print("initialized doom game")

    # lol I'll just leave this episodes thing in
    # lets see what it does
    episodes = 10
    for i in range(episodes):
        print(f"starting doom episode {i}")
        game.new_episode()
        while not game.is_episode_finished():
            state = game.get_state()

            # get frame and convert to PIL
            img = Image.fromarray(state.screen_buffer, "RGB")
            # resize using PIL
            img = img.resize((30, 30), resample=Image.Resampling.NEAREST)
            # back into numpy, and set global framebuffer
            # Convert into numpy array, then into list[list[int,int,int]]
            framebuffer[:] = [[tuple(rgb) for rgb in row] for row in np.array(img)]

            # do thing
            reward = game.make_action(get_current_move())

            time.sleep(0.2)

        time.sleep(2)
