FROM python:3.11

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y cmake git libboost-all-dev libsdl2-dev libopenal-dev kmod kbd
RUN pip install vizdoom pillow keyboard sacn

RUN mkdir /app
COPY ./ /app/

WORKDIR /app
CMD python3 main.py
