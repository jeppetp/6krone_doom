import sacn
import time

def dmx_writing_worker(framebuffer, ip_address, framebuffer_to_dmx):
    sender = sacn.sACNsender()  # provide an IP-Address to bind to if you are using Windows and want to use multicast
    sender.start()  # start the sending thread
    
    for i in range(1,6):
        sender.activate_output(i)
        sender[i].destination = ip_address

    while True:
        time.sleep(0.1)
        dmx_list = framebuffer_to_dmx(framebuffer)
        universes = [dmx_list[(510 * i):(510 * (i+1))] for i in range(5)]
        for i in range(1,6):
            sender[i].dmx_data = universes[i-1]

        time.sleep(0.01)
