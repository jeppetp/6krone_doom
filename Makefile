docker-run: docker-build
	docker run -it \
		-v `pwd`:/app \
		-v /proc:/proc \
		--privileged \
		vizdoom

docker-build:
	docker build . -t vizdoom
